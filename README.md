<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Cert Manager Repo](#cert-manager-repo)
  - [Adding Cert-Manager to a Kubernetes Cluster](#adding-cert-manager-to-a-kubernetes-cluster)
    - [HTTP acme solver](#http-acme-solver)
    - [DNS01](#dns01)
  - [Updating Versions of Cert-Manager](#updating-versions-of-cert-manager)
- [Using Cert Manager](#using-cert-manager)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Cert Manager Repo

This cert-manager repo holds yaml files which contain kubernetes definitions, and it contains automation for pulling [cert-manager](https://cert-manager.io/) and deploying Let's Encrypt to manage certs.

These definitions and scripts are then published to artifactory, and consumed by our [ansible automation](https://gitlab.com/TransparentIncDevelopment/product/devops/ansible) during normal deployment. You can also manually deploy a cert-manager to a kubernetes cluster using the provided `make` targets and scripts, as explained below.

## Adding Cert-Manager to a Kubernetes Cluster

From the cert-manager [documentation](https://cert-manager.io/docs/configuration/acme/)

> In order for the ACME CA server to verify that a client owns the domain, or domains, a certificate
> is being requested for, the client must complete “challenges”. This is to ensure clients are
> unable to request certificates for domains they do not own and as a result, fraudulently
> impersonate another’s site. As detailed in the RFC8555, cert-manager offers two challenge
> validations - HTTP01 and DNS01 challenges.

There is a `Makefile` in this repo for packaging up the necessary resources to deploy an ACME CA cert-manager. It defines `make` targets to build an `output/` directory containing all the files and tools needed to add a cert-manager to a Kubernetes cluster.

### HTTP ACME solver

This script block deploys a cert-manager set up to solve HTTP01 challenges. This will
work if the 3rd party ACME CA server is able to ping your domain (i.e, your domain is not behind any
whitelist or firewalls)

Build and deploy these definitions with:
```
make build-http
cd output
./deploy-cert-manager.py
```

### DNS01
TPFS' develoment clusters are behind a whitelist, so the 3rd party ACME CA server cannot ping them
to complete challenges, and issue certificates.

The `/k8s/dns/` folder contains the configuration and patch for the cert-manager to solve DNS01 challenges.

This configuration requires a service account that is able to update DNS records for the given domain.
In our case, we provide a GCP service account, and configure the the cert-manager application with a
`clouddns`-specific "solver".

For the `develoment` cluster in the `xand-dev` GCP project, overwrite the default patch build and deploy with:

```
make build-dns
cd output

pushd k8s/issuer
# Load and incorporate the gcloud service account as a secret
gcloud iam service-accounts keys create lets-encrypt-dns01-solver-key.json --iam-account lets-encrypt-dns01-solver@xand-dev.iam.gserviceaccount.com
kustomize edit add secret lets-encrypt-dns01-solver-service-account --from-file lets-encrypt-dns01-solver-key.json
popd

./deploy-cert-manager.py
```


## Updating Version of Cert-Manager

The version of [cert-manager](https://github.com/cert-manager/cert-manager/releases) can be updated by setting the
`EXTERNAL_CERT_MANAGER_VERSION` variable towards the top of the `Makefile` to the desired version.
Make sure to choose a version that is compatible with the version of Kubernetes you're running (check 
that [here](https://cert-manager.io/docs/installation/supported-releases/)).

The version of this repo is versioned differently than the external version of Cert-manager. This repo is versioned via
the `version.txt` in the repo root. This **must** be updated when you make changes to this repo.

# Using Cert Manager

To use the cert-manager with an nginx ingress would be to set the following.

Add `kubernetes.io/tls-acme: "true"` to automatically configure the ingress with a cert.

This will automatically create a cert with the host name except with replacing `.` with `-`. So `tools.xand.me` becomes `tools-xand-me`. See below for an example ingress.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: prometheus
  namespace: monitoring
  annotations:
    kubernetes.io/tls-acme: "true"
    kubernetes.io/ingress.class: "nginx"
spec:
  tls:
    - hosts:
        - tools.xand.me
      secretName: tools-xand-me
  rules:
    - host: tools.xand.me
      http:
        paths:
          - backend:
              service:
                name: prometheus-service
                port:
                  number: 8080
            path: /
            pathType: ImplementationSpecific
```
